const apiUrl = 'https://api.example.com/users'; // Reemplaza con tu URL real
const userId = 123; // Reemplaza con el ID que deseas enviar

fetch(`${apiUrl}/${userId}`)
  .then(response => {
    if (!response.ok) {
      throw new Error(`Error de red - ${response.status}`);
    }
    return response.json();
  })
  .then(data => {
    console.log('Datos recibidos:', data);
    // Aquí puedes manejar los datos como desees
  })
  .catch(error => {
    console.error('Error en la solicitud:', error);
  });
