<!DOCTYPE html>
<html lang="en"><!-- Basic -->

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<!-- Mobile Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- Site Metas -->
	<title>Cadena Productiva Cacao</title>
	<meta name="keywords" content="">
	<meta name="description" content="">
	<meta name="author" content="">

	<!-- Site Icons -->
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
	<link rel="apple-touch-icon" href="images/apple-touch-icon.png">

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<!-- Site CSS -->
	<link rel="stylesheet" href="css/style.css">
	<!-- Responsive CSS -->
	<link rel="stylesheet" href="css/responsive.css">
	<!-- Custom CSS -->
	<link rel="stylesheet" href="css/custom.css">



</head>

<body>
	<!-- Start header -->
	<?php include 'view/menu.php'; ?>
	<!-- End header -->

	<!-- Start header -->
	<div class="all-page-title page-breadcrumb">
		<div class="container text-center">
			<div class="row">
				<div class="col-lg-12">
					<h1>iNocuo - Software de trazabilidad y Monitoreo de la Fermentación</h1>
				</div>
			</div>
		</div>
	</div>
	<!-- End header -->

	<!-- Start About -->
	<div class="about-section-box">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 col-md-6">
					<img src="images/about-img.jpg" alt="" class="img-fluid">
				</div>
				<div class="col-lg-6 col-md-6 text-center">
					<div class="inner-column">
						<h1>Trazabilidad global y <span> Monitoreo en la Fermentación del Cacao</span></h1>
						<iframe width="560" height="335" src="https://www.youtube.com/embed/f9PRHb-PLBw?si=cjoB61aThjzYWaEu" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

					</div>
				</div>
				<div class="col-md-12">
					<div class="inner-pt">
						<!-- Start Customer Reviews -->
						<div class="customer-reviews-box">
							<div class="container">
								<div class="row">
									<div class="col-lg-12">
										<div class="heading-title">
											<h2> Utilidades del software </h2>
											<p>Presentamos nuestro software de trazabilidad para el proceso agroindustrial del cacao. Desde la siembra hasta la comercialización, este sistema garantiza transparencia y autenticidad. Cada fase, desde la producción hasta la transformación, se registra meticulosamente. Asignamos códigos QR únicos a cada producto final, permitiendo a los consumidores acceder fácilmente a información detallada sobre la procedencia del cacao y las prácticas sostenibles. Descubre la historia detrás de cada barra de chocolate y únete a nosotros en la promoción de la autenticidad y sostenibilidad en la industria del cacao.</p>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-8 mr-auto ml-auto text-center">
										<div id="reviews" class="carousel slide" data-ride="carousel">
											<div class="carousel-inner mt-4">
												<div class="carousel-item text-center active">
													<div class="img-box p-1 border rounded-circle m-auto">
														<img class="d-block w-100 rounded-circle" src="images/eslabon1.jpg" alt="">
													</div>
													<h5 class="mt-4 mb-0"><strong class="text-warning text-uppercase">Procesos Agricolas</strong></h5>
													<h6 class="text-dark m-0">Eslabon 1</h6>
													<p class="m-0 pt-3">
														En el nivel primario, se incluyen diversas funcionalidades, como el registro de ingreso a la finca con variables climáticas y del suelo. Además, abarca la gestión de viveros, control del terreno, planificación y seguimiento de siembras, supervisión de cosechas, coordinación de la fermentación con una red de sensores Iot en la finca y en acopio, administración de sensores y dispositivos para la fermentación, así como la generación de órdenes de acopio para las guías de despacho</p>
												</div>
												<div class="carousel-item text-center">
													<div class="img-box p-1 border rounded-circle m-auto">
														<img class="d-block w-100 rounded-circle" src="images/eslabon2.jpg" alt="">
													</div>
													<h5 class="mt-4 mb-0"><strong class="text-warning text-uppercase">Procesos Industriales</strong></h5>
													<h6 class="text-dark m-0">Eslabón 2</h6>
													<p class="m-0 pt-3">En el segundo eslabón, se lleva a cabo la transformación del grano seco de cacao. En esta etapa, se realiza el recibo siguiendo la norma ICONTEC NTC 1252, y el software verifica automáticamente el cumplimiento de dicha normativa. Además, se ejecuta el proceso de transformación del cacao en bombones artesanales premium.</p>
												</div>
												<div class="carousel-item text-center">
													<div class="img-box p-1 border rounded-circle m-auto">
														<img class="d-block w-100 rounded-circle" src="images/eslabon3.jpg" alt="">
													</div>
													<h5 class="mt-4 mb-0"><strong class="text-warning text-uppercase">Comercialización</strong></h5>
													<h6 class="text-dark m-0">Eslabon 3</h6>
													<p class="m-0 pt-3">
														En el tercer eslabón, se crea un código QR exclusivo para cada lote de productos elaborados. El objetivo principal es ofrecer toda la información esencial para las estrategias de marketing en relación con la trazabilidad. Este código proporciona un acceso eficaz a detalles clave, como el proceso de producción, el origen del cacao y otros datos pertinentes, asegurando así transparencia y generando confianza entre los consumidores.</p>
												</div>
											</div>
											<a class="carousel-control-prev" href="#reviews" role="button" data-slide="prev">
												<i class="fa fa-angle-left" aria-hidden="true"></i>
												<span class="sr-only">Previous</span>
											</a>
											<a class="carousel-control-next" href="#reviews" role="button" data-slide="next">
												<i class="fa fa-angle-right" aria-hidden="true"></i>
												<span class="sr-only">Next</span>
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- End Customer Reviews -->
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End About -->

	<div class="division">
		<p>"Un buen software de trazabilidad es como un detective que sigue el rastro de cada producto.</p>
		<p> <b>-Anonimo</b></p>
	</div>

	<!-- Start Stuff -->
	<div class="stuff-box">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="heading-title text-center">
						<h2>Equipo De Trabajo</h2>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-3 col-sm-3">
					<div class="our-team">
						<img src="images/juan.jpg">
						<div class="team-content">
							<h3 class="title">Juan José Guzman Pineda</h3>
							<span class="post">Ingeniero de sistemas | Investigador Principal</span>
							<ul class="social">
								<li><a href="https://www.linkedin.com/in/juan-j-guzm%C3%A1n-b1b412201/"><i class="fa fa-linkedin"></i></a></li>

							</ul>
						</div>
					</div>
				</div>
				<div class="col-md-3 col-sm-3">
					<div class="our-team">
						<img src="images/camilo.jpg">
						<div class="team-content">
							<h3 class="title">Camilo Andres Parra Urueta</h3>
							<span class="post">Ingeniero de sistema | Asp. Magister ingenieria de software</span>
							<ul class="social">
								<li><a href="https://www.linkedin.com/in/camilo-andres-parra-370573237/"><i class="fa fa-linkedin"></i></a></li>

							</ul>
						</div>
					</div>
				</div>
				<div class="col-md-3 col-sm-3">
					<div class="our-team">
						<img src="images/sebas.jpg">
						<div class="team-content">
							<h3 class="title">Sebastian Parra Portillo</h3>
							<span class="post">Ingeniero de Sistema</span>
							<ul class="social">
								<li><a href="https://www.linkedin.com/in/sebastian-parra-40417630b/"><i class="fa fa-linkedin"></i></a></li>

							</ul>
						</div>
					</div>
				</div>
				<div class="col-md-3 col-sm-3">
					<div class="our-team">
						<img src="images/doval.jpg">
						<div class="team-content">
							<h3 class="title">Juan Doval</h3>
							<span class="post">Ingeniero de Sistema</span>
							<ul class="social">
								<li><a href="https://www.linkedin.com/in/juan-camilo-doval-sanchez-62388a300/recent-activity/all/"><i class="fa fa-linkedin"></i></a></li>

							</ul>
						</div>
					</div>
				</div>



			</div>
		</div>
	</div>
	<!-- End Stuff -->






	<!-- Start Contact info -->
	<?php include 'view/Contactinfo.php'; ?>
	<!-- End Contact info -->

	<!-- Start Footer -->
	<?php include 'view/footer.php'; ?>
	<!-- End Footer -->

	<a href="#" id="back-to-top" title="Back to top" style="display: none;">&uarr;</a>

	<!-- ALL JS FILES -->
	<script src="js/jquery-3.2.1.min.js"></script>
	<script src="js/popper.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<!-- ALL PLUGINS -->
	<script src="js/jquery.superslides.min.js"></script>
	<script src="js/images-loded.min.js"></script>
	<script src="js/isotope.min.js"></script>
	<script src="js/baguetteBox.min.js"></script>
	<script src="js/form-validator.min.js"></script>
	<script src="js/contact-form-script.js"></script>
	<script src="js/custom.js"></script>
</body>

</html>