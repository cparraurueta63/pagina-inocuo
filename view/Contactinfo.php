<?php
echo '
<div class="contact-imfo-box">
		<div class="container">
			<div class="row">

				<div class="col-md-6">
					<i class="fa fa-envelope"></i>
					<div class="overflow-hidden">
						<h4>Email</h4>
						<p class="lead">
							admproyregcacao@correo.unicordoba.edu.co
						</p>
					</div>
				</div>
				<div class="col-md-6">
					<i class="fa fa-map-marker"></i>
					<div class="overflow-hidden">
						<h4>Ubicación</h4>
						<p class="lead">
							Universidad de Cordoba, sede berastegui
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
'
?>