<?php
// Obtén el valor de 'pagina' de la URL
$pagina_actual = isset($_GET['pagina']) ? $_GET['pagina'] : 'inicio';

// Define las opciones del menú
$menu_items = array(
    'inicio' => 'Inicio',
    'obj1' => 'Obj 1',
    'obj2' => 'Obj 2',
    'obj3' => 'Obj 3',
    'obj4' => 'Obj 4',
    'obj5' => 'Obj 5',
    'https://inocuo.cacaounicordoba.com/' => 'Plataforma'
);

// Imprime el encabezado del menú
echo '<header class="top-navbar">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container">
            <a class="navbar-brand" href="index.html">
                <img src="images/logo.png" alt="" />
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbars-rs-food"
                aria-controls="navbars-rs-food" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbars-rs-food">
                <ul class="navbar-nav ml-auto">';

// Itera sobre las opciones del menú y agrega la clase 'active' a la opción actual
foreach ($menu_items as $clave => $valor) {
    $clase_activa = ($clave == $pagina_actual) ? 'active' : '';

    // Determina si el enlace es interno o externo
    if (filter_var($clave, FILTER_VALIDATE_URL)) {
        echo '<li class="nav-item ' . $clase_activa . '"><a class="nav-link" href="' . $clave . '">' . $valor . '</a></li>';
    } else {
        echo '<li class="nav-item ' . $clase_activa . '"><a class="nav-link" href="index.php?pagina=' . $clave . '">' . $valor . '</a></li>';
    }
}

// Cierra el menú
echo '</ul></div></div></nav></header>';
?>
