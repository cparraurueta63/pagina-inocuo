<?php
// index.php

$pagina = isset($_GET['pagina']) ? $_GET['pagina'] : 'inicio';

// Validar o sanitizar la entrada, por ejemplo:
$pagina = preg_replace('/[^a-zA-Z0-9]/', '', $pagina);

$archivo = $pagina . '.php';

if ($pagina === 'inicio' || !file_exists($archivo)) {
    include('inicio.php'); // Página principal
} else {
    include($archivo);
}
?>
