<!DOCTYPE html>
<html lang="en"><!-- Basic -->

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<!-- Mobile Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- Site Metas -->
	<title>Cadena Productiva Cacao</title>
	<meta name="keywords" content="">
	<meta name="description" content="">
	<meta name="author" content="">

	<!-- Site Icons -->
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
	<link rel="apple-touch-icon" href="images/apple-touch-icon.png">

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<!-- Site CSS -->
	<link rel="stylesheet" href="css/style.css">
	<!-- Responsive CSS -->
	<link rel="stylesheet" href="css/responsive.css">
	<!-- Custom CSS -->
	<link rel="stylesheet" href="css/custom.css">



</head>

<body>
	<!-- Start header -->
	<?php include 'view/menu.php'; ?>
	<!-- End header -->

	<!-- Start header -->
	<div class="all-page-title page-breadcrumb">
		<div class="container text-center">
			<div class="row">
				<div class="col-lg-12">
					<h1>Aumentar el conocimiento de las estrategias que permitan mejorar la calidad e inocuidad y del cacao</h1>
				</div>
			</div>
		</div>
	</div>
	<!-- End header -->

	<!-- Start About -->
	<div class="about-section-box">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 col-md-6">
					<img src="images/about-img.jpg" alt="" class="img-fluid">
				</div>
				<div class="col-lg-6 col-md-6 text-center">
					<div class="inner-column">
						<h1>Trazabilidad global y <span> Monitoreo en la Fermentación del Cacao</span></h1>
						<iframe width="560" height="335" src="https://www.youtube.com/embed/f9PRHb-PLBw?si=cjoB61aThjzYWaEu" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

					</div>
				</div>
				<div class="col-md-12">
					<div class="inner-pt">
						<!-- Start Customer Reviews -->
						<div class="customer-reviews-box">
							<div class="container">
								<div class="row">
									<div class="col-lg-12">
										<div class="heading-title">
											<h2> Utilidades del software </h2>
											<p>Presentamos nuestro software de trazabilidad para el proceso agroindustrial del cacao. Desde la siembra hasta la comercialización, este sistema garantiza transparencia y autenticidad. Cada fase, desde la producción hasta la transformación, se registra meticulosamente. Asignamos códigos QR únicos a cada producto final, permitiendo a los consumidores acceder fácilmente a información detallada sobre la procedencia del cacao y las prácticas sostenibles. Descubre la historia detrás de cada barra de chocolate y únete a nosotros en la promoción de la autenticidad y sostenibilidad en la industria del cacao.</p>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-8 mr-auto ml-auto text-center">
										<div id="reviews" class="carousel slide" data-ride="carousel">
											<div class="carousel-inner mt-4">
												<div class="carousel-item text-center active">
													<div class="img-box p-1 border rounded-circle m-auto">
														<img class="d-block w-100 rounded-circle" src="images/profile-1.jpg" alt="">
													</div>
													<h5 class="mt-4 mb-0"><strong class="text-warning text-uppercase">Paul Mitchel</strong></h5>
													<h6 class="text-dark m-0">Web Developer</h6>
													<p class="m-0 pt-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eu sem tempor, varius quam at, luctus dui. Mauris magna metus, dapibus nec turpis vel, semper malesuada ante. Idac bibendum scelerisque non non purus. Suspendisse varius nibh non aliquet.</p>
												</div>
												<div class="carousel-item text-center">
													<div class="img-box p-1 border rounded-circle m-auto">
														<img class="d-block w-100 rounded-circle" src="images/profile-3.jpg" alt="">
													</div>
													<h5 class="mt-4 mb-0"><strong class="text-warning text-uppercase">Steve Fonsi</strong></h5>
													<h6 class="text-dark m-0">Web Designer</h6>
													<p class="m-0 pt-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eu sem tempor, varius quam at, luctus dui. Mauris magna metus, dapibus nec turpis vel, semper malesuada ante. Idac bibendum scelerisque non non purus. Suspendisse varius nibh non aliquet.</p>
												</div>
												<div class="carousel-item text-center">
													<div class="img-box p-1 border rounded-circle m-auto">
														<img class="d-block w-100 rounded-circle" src="images/profile-7.jpg" alt="">
													</div>
													<h5 class="mt-4 mb-0"><strong class="text-warning text-uppercase">Daniel vebar</strong></h5>
													<h6 class="text-dark m-0">Seo Analyst</h6>
													<p class="m-0 pt-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eu sem tempor, varius quam at, luctus dui. Mauris magna metus, dapibus nec turpis vel, semper malesuada ante. Idac bibendum scelerisque non non purus. Suspendisse varius nibh non aliquet.</p>
												</div>
											</div>
											<a class="carousel-control-prev" href="#reviews" role="button" data-slide="prev">
												<i class="fa fa-angle-left" aria-hidden="true"></i>
												<span class="sr-only">Previous</span>
											</a>
											<a class="carousel-control-next" href="#reviews" role="button" data-slide="next">
												<i class="fa fa-angle-right" aria-hidden="true"></i>
												<span class="sr-only">Next</span>
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- End Customer Reviews -->
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End About -->

	<!-- Start Stuff -->
	<div class="stuff-box">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="heading-title text-center">
						<h2>Equipo De Trabajo</h2>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-3 col-sm-3">
					<div class="our-team">
						<img src="images/juan.jpg">
						<div class="team-content">
							<h3 class="title">Juan José Guzman Pineda</h3>
							<span class="post">Ingeniero de sistemas | Investigador Principal</span>
							<ul class="social">
								<li><a href="https://www.linkedin.com/in/juan-j-guzm%C3%A1n-b1b412201/"><i class="fa fa-linkedin"></i></a></li>

							</ul>
						</div>
					</div>
				</div>
				<div class="col-md-3 col-sm-3">
					<div class="our-team">
						<img src="images/camilo.jpg">
						<div class="team-content">
							<h3 class="title">Camilo Andres Parra Urueta</h3>
							<span class="post">Ingeniero de sistema | Asp. Magister ingenieria de software</span>
							<ul class="social">
								<li><a href="https://www.linkedin.com/in/camilo-andres-parra-370573237/"><i class="fa fa-linkedin"></i></a></li>

							</ul>
						</div>
					</div>
				</div>
				<div class="col-md-3 col-sm-3">
					<div class="our-team">
						<img src="images/sebas.jpg">
						<div class="team-content">
							<h3 class="title">Sebastian Parra Portillo</h3>
							<span class="post">Aspitante a Ingeniero de Sistema</span>
							<ul class="social">
								<li><a href="#"><i class="fa fa-linkedin"></i></a></li>

							</ul>
						</div>
					</div>
				</div>
				<div class="col-md-3 col-sm-3">
					<div class="our-team">
						<img src="images/doval.jpg">
						<div class="team-content">
							<h3 class="title">Juan Doval</h3>
							<span class="post">Aspitante a Ingeniero de Sistema</span>
							<ul class="social">
								<li><a href="#"><i class="fa fa-linkedin"></i></a></li>

							</ul>
						</div>
					</div>
				</div>



			</div>
		</div>
	</div>
	<!-- End Stuff -->






	<!-- Start Contact info -->
	<?php include 'view/Contactinfo.php'; ?>
	<!-- End Contact info -->

	<!-- Start Footer -->
	<?php include 'view/footer.php'; ?>
	<!-- End Footer -->

	<a href="#" id="back-to-top" title="Back to top" style="display: none;">&uarr;</a>

	<!-- ALL JS FILES -->
	<script src="js/jquery-3.2.1.min.js"></script>
	<script src="js/popper.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<!-- ALL PLUGINS -->
	<script src="js/jquery.superslides.min.js"></script>
	<script src="js/images-loded.min.js"></script>
	<script src="js/isotope.min.js"></script>
	<script src="js/baguetteBox.min.js"></script>
	<script src="js/form-validator.min.js"></script>
	<script src="js/contact-form-script.js"></script>
	<script src="js/custom.js"></script>
</body>

</html>